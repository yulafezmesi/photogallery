# Start PhotoGallery Application (Development)

### 1. Install Gridsome CLI tool if you don't have

`npm install --global @gridsome/cli`

### 2. Clone project from repository

1. `git clone git@gitlab.com:itoldyou/photogallery.git` to clone project
2. `cd photogallery` to open the folder
3. `npm install` to install dependencies.
3. `gridsome develop` to start a local dev server at `http://localhost:8080`


# Start PhotoGallery Application (Production)

1. You can deploy the built content in the dist directory to any static file server
2. `gridsome build` to compiled code into /dist folder
3. You can  previewing locally
     ```#F00
    npm install -g serve
    serve -s dist
    ```
4. You can check also live demo.
    *  https://vuephotogallery.netlify.app/
   
